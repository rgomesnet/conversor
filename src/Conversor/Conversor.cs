﻿namespace ConversorApp
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System.Collections;
    using System.IO;
    using System.Linq;
    using System.Text;
    public class Conversor
    {
        private static StringBuilder sb;
        private static StringWriter sw;
        private static Hashtable hashtable;

        public T Converter<T>(string[] mapaCampos, string[] valores)
        {
            IncializaAtributos();
            string json = CriarJson(mapaCampos, valores);
            return JsonConvert.DeserializeObject<T>(json);
        }

        private static void IncializaAtributos()
        {
            sb = new StringBuilder();
            sw = new StringWriter(sb);
            hashtable = new Hashtable();
        }

        private static string CriarJson(string[] propriedades, string[] valores)
        {
            JObject raiz = new JObject();
            foreach (var propriedade in propriedades)
            {
                int index = propriedades.ToList().IndexOf(propriedade);
                if (index >= valores.Count())
                    break;
                AtribuiValorNaPropriedade(raiz, propriedade, valores[index]);
            }

            return raiz.ToString();
        }

        private static void AtribuiValorNaPropriedade(JObject raiz, string propriedade, string valor)
        {
            if (ContemItensAninhados(propriedade))
            {
                string primeiroItem = ObtemPrimeiroItem(propriedade);
                string caminho = ObtemCaminhoPropriedade(raiz, primeiroItem);
                JObject novoObjetoPropriedade = CriaNovoObjetoPropriedade(raiz, primeiroItem, caminho);
                string proximoItem = propriedade = ObtemProximoItemAninhado(propriedade);
                AtribuiValorNaPropriedade(novoObjetoPropriedade, proximoItem, valor.Trim());
            }
            else
            {
                AdicionaPropriedade(raiz, propriedade, valor);
            }
        }

        private static string ObtemProximoItemAninhado(string propriedade)
        {
            return string.Join(".", propriedade.Split('.').Skip(1).ToArray());
        }

        private static JObject CriaNovoObjetoPropriedade(JObject raiz, string propriedade, string caminhoPropriedade)
        {
            JObject novaPropriedade = new JObject();
            var propriedadeExistente = hashtable[caminhoPropriedade] as JObject;

            if (propriedadeExistente == null)
            {
                hashtable[caminhoPropriedade] = novaPropriedade;
                raiz.Add(propriedade, novaPropriedade);
            }
            else
            {
                novaPropriedade = propriedadeExistente;
            }

            return novaPropriedade;
        }

        private static void AdicionaPropriedade(JObject jobject, string propriedade, string valor)
        {
            jobject.Add(propriedade, valor);
        }

        private static bool ContemItensAninhados(string propriedade)
        {
            return propriedade.Split('.').Count() > 1;
        }

        private static string ObtemPrimeiroItem(string propriedade)
        {
            return propriedade.Split('.').First();
        }

        private static string ObtemCaminhoPropriedade(JObject raiz, string propriedade)
        {
            string caminhoPropriedade = propriedade;
            if (raiz.Parent != null)
            {
                if (raiz.Parent.Path.Contains("'"))
                {
                    int startIndex = raiz.Parent.Path.IndexOf("'") + 1;
                    int length = raiz.Parent.Path.LastIndexOf("'");
                    caminhoPropriedade = $"{raiz.Parent.Path.Substring(startIndex, length - startIndex)}.{propriedade}";
                }
                else
                {
                    caminhoPropriedade = $"{raiz.Parent.Path}.{propriedade}";
                }
            }

            return caminhoPropriedade;
        }
    }
}
