﻿namespace UnitTest.ClassesDeTestes
{
    class ClassePessoa
    {
        public string Nome { get; set; }
        public string Sobrenome { get; set; }

        public ClasseComplexa Complexa { get; set; }

        public ClasseComplexaAninhada ClasseComplexaAninhada { get; set; }
    }
}
