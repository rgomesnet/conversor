using ConversorApp;
using UnitTest.ClassesDeTestes;
using Xunit;

namespace UnitTest
{
    public class ConversorUnitTest
    {
        [Fact]
        public void TesteConversaoClasseSimples()
        {
            string[] mapaCampos = { "Propriedade01", "Propriedade02" };
            string[] valores = { "Valor01", "10" };
            Conversor conversor = new Conversor();
            ClasseSimples s = conversor.Converter<ClasseSimples>(mapaCampos, valores);
            Assert.Equal("Valor01", s.Propriedade01);
            Assert.Equal(10, s.Propriedade02);
        }

        [Fact]
        public void TesteConversaoClasseComplexa()
        {
            string[] mapaCampos = { "ClasseComplexaId", "Simples.Propriedade01", "Simples.Propriedade02" };
            string[] valores = { "1", "Valor01", "10" };
            Conversor conversor = new Conversor();
            ClasseComplexa c = conversor.Converter<ClasseComplexa>(mapaCampos, valores);

            Assert.Equal(1, c.ClasseComplexaId);
            Assert.Equal("Valor01", c.Simples.Propriedade01);
            Assert.Equal(10, c.Simples.Propriedade02);
        }

        [Fact]
        public void TesteConversaoClasseComplexaDoisNos()
        {
            string[] mapaCampos = { "Complexa.ClasseComplexaId", "Complexa.Simples.Propriedade01", "Complexa.Simples.Propriedade02" };
            string[] valores = { "1", "Valor01", "10" };
            Conversor conversor = new Conversor();
            ClasseComplexaAninhada c = conversor.Converter<ClasseComplexaAninhada>(mapaCampos, valores);

            Assert.Equal(1, c.Complexa.ClasseComplexaId);
            Assert.Equal("Valor01", c.Complexa.Simples.Propriedade01);
            Assert.Equal(10, c.Complexa.Simples.Propriedade02);
        }

        [Fact]
        public void TesteConversaoClasseComplexaAninhada()
        {
            string[] mapa = {
                "Nome",
                "Sobrenome",
                "Complexa.ClasseComplexaId",
                "Complexa.Simples.Propriedade01",
                "Complexa.Simples.Propriedade02",
                "ClasseComplexaAninhada.Complexa.ClasseComplexaId",
                "ClasseComplexaAninhada.Complexa.Simples.Propriedade01",
                "ClasseComplexaAninhada.Complexa.Simples.Propriedade02"
            };

            string[] valores = {
                "Renato",
                "Gomes",
                "1",
                "Propriedade01",
                "2",
                "3",
                "Propriedade0101",
                "4" };

            Conversor conversor = new Conversor();
            ClassePessoa pessoa = conversor.Converter<ClassePessoa>(mapa, valores);

            Assert.Equal("Renato", pessoa.Nome);
            Assert.Equal("Gomes", pessoa.Sobrenome);

            Assert.Equal(1, pessoa.Complexa.ClasseComplexaId);
            Assert.Equal("Propriedade01", pessoa.Complexa.Simples.Propriedade01);
            Assert.Equal(2, pessoa.Complexa.Simples.Propriedade02);

            Assert.Equal(3, pessoa.ClasseComplexaAninhada.Complexa.ClasseComplexaId);
            Assert.Equal("Propriedade0101", pessoa.ClasseComplexaAninhada.Complexa.Simples.Propriedade01);
            Assert.Equal(4, pessoa.ClasseComplexaAninhada.Complexa.Simples.Propriedade02);
        }
    }
}
